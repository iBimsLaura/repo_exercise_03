﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{


    public class Test : MonoBehaviour
    {
        private bool isEnemyAlive = true;
        private int hitpoints = 100;
        private float critChance = 10.5f;
        private string message = "Hallo";
        private bool grounded = false;

        public int stamina = 200;
        public bool isEnemyWounded = false;
        public string enemyName = "Herbert";
        public float speed = 50f;
        public bool isEnemyFrozen = false;

        public void Start()
        {
            Debug.Log(isEnemyAlive);
            Debug.Log(hitpoints);
            Debug.Log(critChance);
            Debug.Log(message);
            Debug.Log(grounded);
            Debug.Log(stamina);
            Debug.Log(isEnemyWounded);
            Debug.Log(enemyName);
            Debug.Log(speed);
            Debug.Log(isEnemyFrozen);
        }


    }



}

