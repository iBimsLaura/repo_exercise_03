﻿using UnityEngine;
using UnityEditor;

namespace CoAHomework
{
    public class Player : MonoBehaviour
    {
        public int poisonDamage = 30;
        public string playerName = "Ursula";
        public float deflectChance = 30.5f;
        public bool isPlayerAttacking = false;
        public bool isPlayerDucking = true;

        public void Start()
        {
            Debug.Log(poisonDamage);
            Debug.Log(playerName);
            Debug.Log(deflectChance);
            Debug.Log(isPlayerAttacking);
            Debug.Log(isPlayerDucking);
        }
    }
}

